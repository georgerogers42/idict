class IDict(object):
    def __init__(self, data=[]):
        self._klist = []
        self._dict = {}
        for k, v in data:
            self[k] = v
    def __getitem__(self, key):
        return self._dict[key]
    def get(self, key, default=None):
        return self._dict.get(key, default)
    def __setitem__(self, key, value):
        if key not in self._dict:
            self._klist.append(key)
        self._dict[key] = value
    def __delitem__(self, key):
        self._klist.remove(key)
        del self._dict[key]
    def __iter__(self):
        return iter(self._klist)
    def items(self):
        for k in self._klist:
            yield k, self[k]
    def copy(self):
        return IDict(self.items())

if __name__ == "__main__":
    import re
    from sys import stdin
    WORDS = re.compile(r"\W+")
    def main():
        d = IDict()
        for line in stdin:
            for word in WORDS.split(line):
                if word != "":
                    d[word] = d.get(word, 0) + 1
        for k, v in d.items():
            print("%s: %d" % (k, v))
    main()
